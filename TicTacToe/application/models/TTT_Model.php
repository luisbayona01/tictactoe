<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class TTT_Model extends CI_Model
	{
		//Función que envía los nombres de los jugadores a la base de datos
		public function submit()
		{
			$player1 = $this->input->post("player1name");
			$player2 = $this->input->post("player2name");
			var_dump($player2);
			if($this->input->post("player1name") != $this->input->post("player2name")) //Comprueba si el nombre del jugador 1 es el mismo que el nombre del jugador 2
			{
				$data = array(
					"player1name" => $this->input->post("player1name"),
					"player2name" => $this->input->post("player2name")
				);	

				$this->db->insert('results', $data);
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Problem With Entering Names');
				redirect(base_url('TTT_Controller/index'));
				return false;
			}
		}

		//Function that gets the last names that were entered into the database; used in the game_veiw to have the player names in messages
		public function getPlayerNames()
		{
			$this->db->order_by('id', 'desc');
			$this->db->limit(1);
			$query = $this->db->get('results');

			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}

		//Función que obtiene los apellidos que se ingresaron en la base de datos; usado en game_veiw para tener los nombres de los jugadores en los mensajes
		public function posting_results()
		{
			$this->db->order_by('id', 'desc');
			$this->db->limit(1);

			if($this->input->post('winner') == "")
			{
				$cancelled = "Cancelled game";
				$data = array(
					'winner' => $cancelled
				);

			}
			else
			{
				$data = array(
					'winner' => $this->input->post('winner')
				);	
			}
			
			$this->db->update('results', $data);
		}

		//Función utilizada para la paginación para obtener todas las filas en la tabla de "resultados"
		public function results_count()
		{
			return $this->db->count_all("results");
		}

		//Obtiene todos los resultados de la tabla "resultados". Utilizado para la paginación
		public function fetch_results($limit, $start)
		{
			$this->db->limit($limit, $start);
			$query = $this->db->get("results");

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $row) 
				{
					$data[]=$row;
				}
				return $data;
			}
			else
			{
				return false;
			}
		}

		//Función utilizada para obtener las últimas 5 coincidencias de la tabla. Se utiliza para mostrar los datos obtenidos en la vista player_name_view
		public function getLastFiveMatches()
		{
			$this->db->order_by('id', 'desc');
			$this->db->limit(5);
			$query = $this->db->get('results');

			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}

		//Función utilizada para enviar el nombre del jugador que se enfrentará a la IA; Además, el nombre del jugador AI se envía a la base de datos.
		public function submit1P()
		{
			$player1 = $this->input->post("player1name");
			$player2 = "AI_John";
			
			$data = array(
				"player1name" => $this->input->post("player1name"),
				"player2name" => $player2
			);	

			$this->db->insert('results', $data);
		}
	}
?>