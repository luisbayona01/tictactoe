<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class TTT_Controller extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('TTT_Model', 'model');	//hace el nombre de llamada del modelo TTT_Model
		}

		//Hace que la vista del nombre del jugador sea la vista predeterminada cuando se llama al controlador
		public function index()
		{
			$names['games'] = $this->model->getLastFiveMatches();  //carga los últimos 5 partidos para que se puedan mostrar en la vista
			$this->load->view('player_name_view', $names);	
		}
		
		//esta función se llama cuando se hace clic en el botón de reproducción en la vista player_name_view; llama a la función de envío en TTT_Model para enviar los datos (nombres de los jugadores) a la base de datos
		public function submit()
		{
			$result = $this->model->submit();
			
			redirect(base_url('TTT_Controller/game', $result));
		}

		//Función para mostrar la vista game_view; los nombres de los jugadores que se enviaron en la vista player_name_view se transmiten en la llamada de vista para que los mensajes puedan mostrar quién es el turno y quién ganó (a menos que haya un empate)
		public function game()
		{
			$names['games'] = $this->model->getPlayerNames();
			$this->load->view('game_view', $names);
		}

		//Envía los resultados de la coincidencia a la base de datos utilizando la función post_results TTT_Model
		public function posting_results()
		{
			$result = $this->model->posting_results();
			
			redirect(base_url('TTT_Controller/results', $result));
		}

		//Función que muestra los resultados de los partidos anteriores; usa la paginación CodeIgniter
		public function results()
		{
			$config = array();
			$config["base_url"] = base_url(). "TTT_Controller/results";
			$config["total_rows"] = $this->model->results_count();
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;

			$this->pagination->initialize($config);

			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data["results"] = $this->model->fetch_results($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();

			$this->load->view('results_view', $data);
		}

		//Función que envía el nombre del jugador a la base de datos en caso de que se decida una coincidencia de IA
		public function submitVsCpu()
		{
			$result = $this->model->submit1P();

			redirect(base_url('TTT_Controller/gameVsCpu', $result));
		}

		//Función que permite la visualización de la vista game_vs_cpu_view; tanto el nombre del jugador como la IA se transmiten para mensajes (turno y victoria)
		public function gameVsCpu()
		{
			$names['games'] = $this->model->getPlayerNames();
			$this->load->view('game_vs_cpu_view', $names);
		}
	}
?>